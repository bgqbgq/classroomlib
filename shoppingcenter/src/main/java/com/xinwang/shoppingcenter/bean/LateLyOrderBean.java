package com.xinwang.shoppingcenter.bean;

import com.xinwang.bgqbaselib.http.CommonEntity;

import java.util.List;

/**
 * Date:2021/8/6
 * Time;11:39
 * author:baiguiqiang
 */
public class LateLyOrderBean extends CommonEntity {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1482
         * user_id : 4833
         * order_id : 1024
         * goods_id : 65
         * sku_id : 143
         * num : 8
         * price : 1600
         * admin_tips :
         * waybill_id : 0
         * state : 2
         * pay_state : 1
         * waybill_state : 1
         * cancel_state : 2
         * refund_state : 1
         * review_state : 1
         * user : {"id":4833,"phone":"19180732819","avatar":"http://oss.xw518app.xw518.com/2021-05-14/ff7a5fd3ee1a46ffae7e844154b58fd34833zy.png","nickname":"兴旺药业邱老师","jifen":450,"profile":"","badge":"技术服务老师","create_time":"2020-11-25 13:28:37","state":1,"wxid":"","unionid":"","inviter_id":0,"inviter_time":0,"is_official":1,"reg_time":1606282117,"launch_time":1628214031,"vip":0,"sex":0,"is_erp":0}
         * goods : {"id":65,"category_id":39,"title":"热痛宁（卡巴匹林钙可溶性粉）","cover":"http://oss.xw518app.xw518.com/default/2021/08/05/164723_热痛宁.jpg","pics":"[]","sku":"规格","state":1,"video":"","min_price":1600,"buy_verify":0,"rank":1,"click":17,"review_count":0,"attr0":"[]","attr1":"[]","attr2":"","attr3":"","attr4":"","attr5":"","attr6":"","attr7":"","attr8":"","attr9":""}
         * sku : {"id":143,"goods_id":65,"price":1600,"stock":999999,"allow_coupon":1,"state":1,"sku0":"100g/袋","sku1":"","sku2":"","sku3":"","sku4":"","sku5":"","sku6":"","sku7":"","sku8":"","sku9":"","cover":""}
         * order : {"id":1024,"order_no":"20210805164055000012e100000400","user_id":4833,"tel":"191****2819","area_code":310101,"address":"上海市 市辖区 黄浦区 jjdj\n","nickname":"jdjdk","tips":"","create_time":1628152855,"price":14300,"item_price":12800,"coupon_price":0,"total_price":14300,"reduction_price":0,"post_price":1500,"pay_state":1,"admin_tips":"","cancel_state":2,"state":2,"pay_type":null,"pay_time":0}
         */

        private int id;
        private int user_id;



        private long price;
        private int state;
        private UserBean user;
        private GoodsBean goods;
        private SkuBean sku;
        private OrderBean order;

        public int getId() {
            return id;
        }

        public DataBean(int id, long price, UserBean user, GoodsBean goods,OrderBean orderBean) {
            this.id = id;
            this.price = price;
            this.user = user;
            this.goods = goods;
            this.order =orderBean;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }




        public long getPrice() {
            return price;
        }

        public void setPrice(long price) {
            this.price = price;
        }




        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }







        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public GoodsBean getGoods() {
            return goods;
        }

        public void setGoods(GoodsBean goods) {
            this.goods = goods;
        }

        public SkuBean getSku() {
            return sku;
        }

        public void setSku(SkuBean sku) {
            this.sku = sku;
        }

        public OrderBean getOrder() {
            return order;
        }

        public void setOrder(OrderBean order) {
            this.order = order;
        }

        public static class UserBean {
            public UserBean(int id, String nickname) {
                this.id = id;
                this.nickname = nickname;
            }

            /**
             * id : 4833
             * phone : 19180732819
             * avatar : http://oss.xw518app.xw518.com/2021-05-14/ff7a5fd3ee1a46ffae7e844154b58fd34833zy.png
             * nickname : 兴旺药业邱老师
             * jifen : 450
             * profile :
             * badge : 技术服务老师
             * create_time : 2020-11-25 13:28:37
             * state : 1
             * wxid :
             * unionid :
             * inviter_id : 0
             * inviter_time : 0
             * is_official : 1
             * reg_time : 1606282117
             * launch_time : 1628214031
             * vip : 0
             * sex : 0
             * is_erp : 0
             */

            private int id;
            private String phone;
            private String avatar;
            private String nickname;

            private String profile;
            private String badge;
            private String create_time;

            private String wxid;
            private String unionid;


            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }


            public String getProfile() {
                return profile;
            }

            public void setProfile(String profile) {
                this.profile = profile;
            }

            public String getBadge() {
                return badge;
            }

            public void setBadge(String badge) {
                this.badge = badge;
            }

            public String getCreate_time() {
                return create_time;
            }

            public void setCreate_time(String create_time) {
                this.create_time = create_time;
            }



            public String getWxid() {
                return wxid;
            }

            public void setWxid(String wxid) {
                this.wxid = wxid;
            }

            public String getUnionid() {
                return unionid;
            }

            public void setUnionid(String unionid) {
                this.unionid = unionid;
            }


        }

        public static class GoodsBean {
            public GoodsBean(int id, String title) {
                this.id = id;
                this.title = title;
            }

            /**
             * id : 65
             * category_id : 39
             * title : 热痛宁（卡巴匹林钙可溶性粉）
             * cover : http://oss.xw518app.xw518.com/default/2021/08/05/164723_热痛宁.jpg
             * pics : []
             * sku : 规格
             * state : 1
             * video :
             * min_price : 1600
             * buy_verify : 0
             * rank : 1
             * click : 17
             * review_count : 0
             * attr0 : []
             * attr1 : []
             * attr2 :
             * attr3 :
             * attr4 :
             * attr5 :
             * attr6 :
             * attr7 :
             * attr8 :
             * attr9 :
             */

            private int id;
            private int category_id;
            private String title;
            private String cover;
            private String pics;
            private String sku;
            private int state;
            private String video;

            private int click;


            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getCategory_id() {
                return category_id;
            }

            public void setCategory_id(int category_id) {
                this.category_id = category_id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getCover() {
                return cover;
            }

            public void setCover(String cover) {
                this.cover = cover;
            }

            public String getPics() {
                return pics;
            }

            public void setPics(String pics) {
                this.pics = pics;
            }

            public String getSku() {
                return sku;
            }

            public void setSku(String sku) {
                this.sku = sku;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }

            public String getVideo() {
                return video;
            }

            public void setVideo(String video) {
                this.video = video;
            }



            public int getClick() {
                return click;
            }

            public void setClick(int click) {
                this.click = click;
            }


        }

        public static class SkuBean {
            /**
             * id : 143
             * goods_id : 65
             * price : 1600
             * stock : 999999
             * allow_coupon : 1
             * state : 1
             * sku0 : 100g/袋
             * sku1 :
             * sku2 :
             * sku3 :
             * sku4 :
             * sku5 :
             * sku6 :
             * sku7 :
             * sku8 :
             * sku9 :
             * cover :
             */

            private int id;
            private long price;
            private int state;
            private String cover;
            public int getId() {
                return id;
            }
            public void setId(int id) {
                this.id = id;
            }
            public long getPrice() {
                return price;
            }
            public void setPrice(long price) {
                this.price = price;
            }
            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }



            public String getCover() {
                return cover;
            }

            public void setCover(String cover) {
                this.cover = cover;
            }
        }

        public static class OrderBean {
            public OrderBean(int create_time) {
                this.create_time = create_time;
            }

            /**
             * id : 1024
             * order_no : 20210805164055000012e100000400
             * user_id : 4833
             * tel : 191****2819
             * area_code : 310101
             * address : 上海市 市辖区 黄浦区 jjdj
             * nickname : jdjdk
             * tips :
             * create_time : 1628152855
             * price : 14300
             * item_price : 12800
             * coupon_price : 0
             * total_price : 14300
             * reduction_price : 0
             * post_price : 1500
             * pay_state : 1
             * admin_tips :
             * cancel_state : 2
             * state : 2
             * pay_type : null
             * pay_time : 0
             */

            private int id;
            private int user_id;
            private String nickname;
            private int create_time;
            private long price;

            private int state;


            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }


            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }



            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }


            public int getCreate_time() {
                return create_time;
            }

            public void setCreate_time(int create_time) {
                this.create_time = create_time;
            }

            public long getPrice() {
                return price;
            }

            public void setPrice(long price) {
                this.price = price;
            }




            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }

        }
    }
}
