package com.xinwang.bgqbaselib.utils;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Method;

/**
 *  异常不崩溃
 * Date:2021/11/15
 * Time;17:16
 * author:baiguiqiang
 */
public class ExceptionHandlerUtil {
    private static Application consApplication;
    public static void init(Application application){
        (new Handler(application.getMainLooper())).post((Runnable)(new Runnable() {
            public final void run() {
                while(true) {
                    try {
                        Looper.loop();
                    } catch (Throwable var2) {
                       setError(var2);
                    }
                }
            }
        }));
        Thread.setDefaultUncaughtExceptionHandler((Thread.UncaughtExceptionHandler)(new Thread.UncaughtExceptionHandler() {
            public final void uncaughtException(Thread t, Throwable ex) {

                setError(ex);
            }
        }));
        consApplication = getConsApplicationInstance((Context)application);

        try {
            Method var10000 = Application.class.getDeclaredMethod("attach", Context.class);
            // Intrinsics.checkExpressionValueIsNotNull(var10000, "Application::class.java.…ch\", Context::class.java)");
            Method method = var10000;
            if (method != null) {
                method.setAccessible(true);
                method.invoke(consApplication, consApplication.getBaseContext());
            }
        } catch (Exception var3) {
            var3.printStackTrace();
        }
    }
    private final static Application getConsApplicationInstance(Context paramContext) {
        try {
            if (consApplication == null) {
                ClassLoader classLoader = paramContext.getClassLoader();
                if (classLoader != null) {
                    Class mClass = classLoader.loadClass(Application.class.getName());
                    if (mClass != null) {
                        Object var10001 = mClass.newInstance();
                        if (var10001 == null) {
                            throw new Exception("null cannot be cast to non-null type android.app.Application");
                        }

                        consApplication = (Application)var10001;
                    }
                }
            }
        } catch (Exception var4) {
            var4.printStackTrace();
        }

        return consApplication;
    }
    private final static void setError(Throwable ex) {
        if (ex != null) {
            StringBuffer sb = new StringBuffer();
            StringWriter writer = new StringWriter();
            PrintWriter pw = new PrintWriter((Writer)writer);
            ex.printStackTrace(pw);

            for(Throwable cause = ex.getCause(); cause != null; cause = cause.getCause()) {
                cause.printStackTrace(pw);
            }

            pw.close();
            String var10000 = writer.toString();

            String result = var10000;
            sb.append(result);
            Log.e("TQQ", (new StringBuilder()).append("error: ").append(sb).toString());
          /*val intent = Intent(instance, ErrorActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("content", sb.toString())
            instance!!.startActivity(intent)*/
        }

    }
}
