package com.xingwang.classroomlib;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xinwang.bgqbaselib.base.BaseLazyLoadFragment;
import com.xinwang.shoppingcenter.ui.ShoppingHomeFragment;

/**
 * Date:2021/10/22
 * Time;10:01
 * author:baiguiqiang
 */
public class Test2Fragmnet extends BaseLazyLoadFragment {
    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(com.xingwang.classroom.R.layout.fragment_text2_classroom,container,false);

        return view;
    }

    @Override
    public void initData() {

    }

    @Override
    protected void onInvisible() {
        super.onInvisible();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }
}
