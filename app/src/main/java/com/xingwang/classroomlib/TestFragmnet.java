package com.xingwang.classroomlib;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xinwang.bgqbaselib.base.BaseLazyLoadFragment;
import com.xinwang.shoppingcenter.ui.ShoppingHomeFragment;

/**
 * Date:2021/10/22
 * Time;10:01
 * author:baiguiqiang
 */
public class TestFragmnet extends BaseLazyLoadFragment {
    ShoppingHomeFragment shoppingHomeFragment;
    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        shoppingHomeFragment = new ShoppingHomeFragment();
        View view =  inflater.inflate(com.xingwang.classroom.R.layout.fragment_text_classroom,container,false);
        getChildFragmentManager().beginTransaction().add(R.id.flameLayout,shoppingHomeFragment).commit();
        return view;
    }

    @Override
    public void initData() {

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (shoppingHomeFragment!=null)
        shoppingHomeFragment.onHiddenChanged(hidden);

    }
}
