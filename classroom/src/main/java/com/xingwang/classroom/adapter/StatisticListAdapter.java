package com.xingwang.classroom.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.beautydefinelibrary.BeautyDefine;
import com.xingwang.classroom.R;
import com.xingwang.classroom.bean.BaoJiaBean;
import com.xingwang.classroom.view.CircularImage;
import com.xinwang.bgqbaselib.adapter.BaseLoadMoreAdapter;
import com.xinwang.bgqbaselib.utils.GlideUtils;
import com.xinwang.bgqbaselib.utils.TimeUtil;

import java.util.List;

/**
 * Date:2021/9/15
 * Time;16:18
 * author:baiguiqiang
 */
public class StatisticListAdapter extends BaseLoadMoreAdapter<String> {
    public String mStatTime;

    public StatisticListAdapter(List<String> mDatas, String mStatTime) {
        super(mDatas);
        this.mStatTime = mStatTime;

    }

    @Override
    protected void onBaseBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof StatisticListHolder){
            StatisticListHolder mViewHolder = (StatisticListHolder) viewHolder;

            mViewHolder.tvTime .setText( TimeUtil.byTimeAddDay1(mStatTime, i));

            if (TextUtils.isEmpty(mDatas.get(i))) {
                mViewHolder.tvPrice.setText("暂无数据");
            }else {
                mViewHolder.tvPrice.setText(mDatas.get(i));
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onBaseCreateViewHolder(View view, int viewType) {
        return new StatisticListHolder(view);
    }

    @Override
    public int getViewLayout(int viewType) {
        return R.layout.item_statistic_list_classroom;
    }
    class StatisticListHolder extends RecyclerView.ViewHolder{

        TextView tvTime;
        TextView tvPrice;

        public StatisticListHolder(@NonNull View itemView) {
            super(itemView);

            tvTime =itemView.findViewById(R.id.tvTime);
            tvPrice =itemView.findViewById(R.id.tvPrice);

        }
    }
}
