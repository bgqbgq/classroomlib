package com.xingwang.classroom.adapter;

import android.animation.AnimatorInflater;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xingwang.classroom.R;
import com.xingwang.classroom.bean.LectureListsBean;
import com.xingwang.classroom.bean.MZPZJLListBean;
import com.xinwang.bgqbaselib.adapter.BaseLoadMoreAdapter;
import com.xinwang.bgqbaselib.utils.CommentUtils;
import com.xinwang.bgqbaselib.utils.GlideUtils;

import java.util.List;

/**
 * Date:2019/8/21
 * Time;13:57
 * author:baiguiqiang
 */
public class MZPZJLListAdapter extends BaseLoadMoreAdapter<MZPZJLListBean.DataBean> {

    public MZPZJLListAdapter(List<MZPZJLListBean.DataBean> mDatas) {
        super(mDatas);
    }
    public void  setData(List<MZPZJLListBean.DataBean> mDatas){
       this.mDatas =mDatas;
    }

    @SuppressLint("SetTextI18n")
    @Override
   public void onBaseBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof HomeViewHolder){
            HomeViewHolder mHolder = (HomeViewHolder) viewHolder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mHolder.cvRoot.setStateListAnimator(AnimatorInflater.loadStateListAnimator(mHolder.cvRoot.getContext(),R.animator.animation_item_state_list_classroom));
            }
            mHolder.tvNum.setText("");
            mHolder.tvTip.setText(mDatas.get(i).getTitle());
            mHolder.tvTime.setText(mDatas.get(i).getDate_pz());
        }
    }

    @Override
    public RecyclerView.ViewHolder onBaseCreateViewHolder(View view,int viewType) {
        return new HomeViewHolder(view);
    }

    @Override
    public int getViewLayout(int viewType) {
        return R.layout.item_mzpzjl_list_adapter_classroom;
    }
    class HomeViewHolder extends RecyclerView.ViewHolder{
        RelativeLayout cvRoot;
        TextView tvTip,tvNum,tvTime;
         HomeViewHolder(@NonNull View itemView) {
            super(itemView);
             tvTime= itemView.findViewById(R.id.tvTime);
             tvNum= itemView.findViewById(R.id.tvNum);
             tvTip= itemView.findViewById(R.id.tvTip);
             cvRoot= itemView.findViewById(R.id.cvRoot);
        }
    }
}
