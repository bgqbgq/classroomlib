package com.xingwang.classroom.bean;

import com.xinwang.bgqbaselib.http.CommonEntity;

/**
 * Date:2021/9/13
 * Time;16:30
 * author:baiguiqiang
 */
public class OfferBean extends CommonEntity {
    private Object data;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
