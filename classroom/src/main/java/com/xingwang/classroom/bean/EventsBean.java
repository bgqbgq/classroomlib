package com.xingwang.classroom.bean;

import java.io.Serializable;

/**
 * Date:2021/11/17
 * Time;14:05
 * author:baiguiqiang
 */
public class EventsBean implements Serializable {
    /**
     * id : 76
     * mzpzjl_id : 5
     * user_id : 22
     * event_date : 2022-01-27
     * event_info : 配种后79天-93天 注射伪狂犬疫苗
     */

    private int id;
    private int mzpzjl_id;
    private int user_id;
    private String event_date;
    private String event_info;

    @Override
    public String toString() {
        return "EventsBean{" +
                "id=" + id +
                ", mzpzjl_id=" + mzpzjl_id +
                ", user_id=" + user_id +
                ", event_date='" + event_date + '\'' +
                ", event_info='" + event_info + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMzpzjl_id() {
        return mzpzjl_id;
    }

    public void setMzpzjl_id(int mzpzjl_id) {
        this.mzpzjl_id = mzpzjl_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public String getEvent_info() {
        return event_info;
    }

    public void setEvent_info(String event_info) {
        this.event_info = event_info;
    }
}
