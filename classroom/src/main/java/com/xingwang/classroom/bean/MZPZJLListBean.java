package com.xingwang.classroom.bean;

import com.google.gson.annotations.SerializedName;
import com.xinwang.bgqbaselib.http.CommonEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Date:2021/10/14
 * Time;14:25
 * author:baiguiqiang
 */
public class MZPZJLListBean  extends CommonEntity {


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * id : 5
         * user_id : 22
         * title : 地一次
         * tips : 哈哈哈
         * date_pz : 2021-11-09
         * date_cz : 2022-03-03
         * create_time : 1636424439
         * user : {"id":22,"phone":"15281046619","avatar":"http://oss.xw518app.xw518.com/2020-11-30/e7d3ee51f2884a568885eba8e84cdf3722zy.png","nickname":"哈哈","jifen":1269,"profile":"u乌苏三四死亡ii啊计算机试试看开始看时时刻刻上课困死咔咔咔可是可是上课","badge":"","create_time":"2019-05-22 10:44:06","state":1,"wxid":"o1D6Xw0Nn9TPHAmnrykpkfZUImTM","unionid":"","inviter_id":0,"inviter_time":0,"is_official":0,"reg_time":0,"launch_time":1631687813,"vip":0,"sex":1,"is_erp":2}
         * events : [{"id":76,"mzpzjl_id":5,"user_id":22,"event_date":"2022-01-27","event_info":"配种后79天-93天 注射伪狂犬疫苗"},{"id":77,"mzpzjl_id":5,"user_id":22,"event_date":"2022-02-07","event_info":"配后90天 更换哺乳料"},{"id":78,"mzpzjl_id":5,"user_id":22,"event_date":"2022-02-23","event_info":"配种后106天 上产床"},{"id":79,"mzpzjl_id":5,"user_id":22,"event_date":"2022-03-03","event_info":"配种后114天 产仔"},{"id":80,"mzpzjl_id":5,"user_id":22,"event_date":"2022-03-04","event_info":"产后第1天 滴鼻伪狂犬"},{"id":81,"mzpzjl_id":5,"user_id":22,"event_date":"2022-03-05","event_info":"产后第2天 口服拉稀药品"},{"id":82,"mzpzjl_id":5,"user_id":22,"event_date":"2022-03-06","event_info":"产后第3天 注射升血素"},{"id":83,"mzpzjl_id":5,"user_id":22,"event_date":"2022-03-10","event_info":"产后第7到14天 小猪注射支原体疫苗/圆环疫苗。产后2周母猪注射细小疫苗"},{"id":84,"mzpzjl_id":5,"user_id":22,"event_date":"2022-03-13","event_info":"产后10天 注射升血素"},{"id":85,"mzpzjl_id":5,"user_id":22,"event_date":"2022-03-31","event_info":"产后28天仔猪 注射猪瘟疫苗；母猪猪瘟、口蹄疫分点免疫。"},{"id":86,"mzpzjl_id":5,"user_id":22,"event_date":"2022-03-31","event_info":"产后28-30天 断奶"},{"id":87,"mzpzjl_id":5,"user_id":22,"event_date":"2022-04-07","event_info":"产后35天 注射口蹄疫疫苗"},{"id":88,"mzpzjl_id":5,"user_id":22,"event_date":"2022-04-17","event_info":"产后45天左右 伪狂犬疫苗"},{"id":89,"mzpzjl_id":5,"user_id":22,"event_date":"2022-04-27","event_info":"产后55天左右 猪瘟二免，口蹄疫二免（分点打）"},{"id":90,"mzpzjl_id":5,"user_id":22,"event_date":"2022-05-12","event_info":"产后70天左右 伪狂犬二免"}]
         */

        private int id;
        private String title;
        private String tips;
        private String date_pz;
        private int create_time;
        private UserInfoBean.DataBean user;
        @SerializedName("events")//加了混淆可能出现 解析不到该字段内容
        private List<EventsBean> mEvents;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }



        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTips() {
            return tips;
        }

        public void setTips(String tips) {
            this.tips = tips;
        }

        public String getDate_pz() {
            return date_pz;
        }

        public void setDate_pz(String date_pz) {
            this.date_pz = date_pz;
        }



        public int getCreate_time() {
            return create_time;
        }

        public void setCreate_time(int create_time) {
            this.create_time = create_time;
        }

        public UserInfoBean.DataBean getUser() {
            return user==null?new UserInfoBean.DataBean():user;
        }

        public void setUser(UserInfoBean.DataBean user) {
            this.user = user;
        }

        public List<EventsBean> getEvents() {
            return mEvents==null?new ArrayList<>():mEvents;
        }

        public void setEvents(List<EventsBean> events) {
            this.mEvents = events;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "id=" + id +
                    ", title='" + title + '\'' +
                    ", tips='" + tips + '\'' +
                    ", date_pz='" + date_pz + '\'' +
                    ", create_time=" + create_time +
                    ", user=" + user +
                    ", events=" + mEvents +
                    '}';
        }
    }
}
