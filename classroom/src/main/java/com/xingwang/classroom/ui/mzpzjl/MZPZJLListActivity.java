package com.xingwang.classroom.ui.mzpzjl;


import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import com.beautydefinelibrary.BeautyDefine;
import com.beautydefinelibrary.OpenPageDefine;
import com.google.gson.Gson;
import com.xingwang.classroom.R;
import com.xingwang.classroom.adapter.MZPZJLListAdapter;
import com.xingwang.classroom.bean.MZPZJLListBean;
import com.xingwang.classroom.view.WrapContentLinearLayoutManager;
import com.xinwang.bgqbaselib.adapter.BaseLoadMoreAdapter;
import com.xinwang.bgqbaselib.base.BaseNetActivity;
import com.xinwang.bgqbaselib.dialog.CenterDefineDialog;
import com.xinwang.bgqbaselib.http.ApiParams;
import com.xinwang.bgqbaselib.http.CommonEntity;
import com.xinwang.bgqbaselib.http.HttpCallBack;
import com.xinwang.bgqbaselib.http.HttpUrls;
import com.xinwang.bgqbaselib.utils.LogUtil;
import com.xinwang.bgqbaselib.utils.MyToast;
import com.xinwang.bgqbaselib.utils.TransitionHelper;
import com.xinwang.bgqbaselib.view.CustomToolbar;
import com.xinwang.bgqbaselib.view.VpSwipeRefreshLayout;

import java.io.Serializable;
import java.util.List;

/**
 * Date:2021/10/12
 * Time;11:37
 * author:baiguiqiang
 */
public class MZPZJLListActivity extends BaseNetActivity {
    private FloatingActionButton fab;
    private CustomToolbar toolbar ;
    private RecyclerView recyclerView;
    private String userId;
    private boolean isCurUser =false;//是不是自己
    private VpSwipeRefreshLayout swipeRefreshLayout;
    private MZPZJLListAdapter mAdapter;

    @Override
    protected int layoutResId() {
        return R.layout.activity_mzpzjl_list_classroom;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
        initRecyclerView();
        initSwipeRefreshLayout();
        initData();
        initListener();

    }

    private void initListener() {

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpAddActivity();
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestData(false);
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initSwipeRefreshLayout() {
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setColorSchemeResources(R.color.SwipeRefreshLayoutClassRoom);
    }

    private void initData() {

        Uri uri = getIntent().getData();
        if (uri != null) {
            userId = uri.getQueryParameter("user_id");
        }else
            userId =getIntent().getStringExtra("id");
        if (TextUtils.isEmpty(userId)){
            userId = BeautyDefine.getUserInfoDefine(this).getUserId();
        }
        isCurUser = userId.equals(BeautyDefine.getUserInfoDefine(this).getUserId());
        requestData(false);
        if (!isCurUser){
            ((CoordinatorLayout) findViewById(R.id.coordinator)).removeViewAt(2);
            ((CoordinatorLayout) findViewById(R.id.coordinator)).removeViewAt(1);
        }
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new WrapContentLinearLayoutManager(this));
    }

    private void initViews() {
        fab =findViewById(R.id.fab);
        toolbar =findViewById(R.id.toolbar);

        recyclerView = findViewById(R.id.recyclerView);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
    }

    private void jumpAddActivity(){
        Intent intent = new Intent(this, MZPZJLAddActivity.class);

        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(
                    (this), true,
                    new Pair<>(fab, getResources().getString(R.string.transition_fab_ClassRoom)),new Pair<>(toolbar,getResources().getString(R.string.transition_toolbar_ClassRoom)));
            ActivityCompat.startActivityForResult(this, intent, 100,ActivityOptionsCompat.makeSceneTransitionAnimation(this,pairs).toBundle());
        } else {
            startActivityForResult(intent,100);
        }
        overridePendingTransition(0, 0);
    }
    private void requestData(boolean isAdd){
        if (!isCurUser)
            requestGet(HttpUrls.URL_MZPZJL_LIST2(), new ApiParams().with("user_id", userId), MZPZJLListBean.class, new HttpCallBack<MZPZJLListBean>() {
                @Override
                public void onFailure(String message) {
                    swipeRefreshLayout.setRefreshing(false);
                    MyToast.myToast(getApplicationContext(),message);
                }

                @Override
                public void onSuccess(MZPZJLListBean commonEntity) {
                    swipeRefreshLayout.setRefreshing(false);
                    initShow(commonEntity.getData(),isAdd);
                }
            });
        else
            requestGet(HttpUrls.URL_MZPZJL_LIST(), new ApiParams(), MZPZJLListBean.class, new HttpCallBack<MZPZJLListBean>() {
                @Override
                public void onFailure(String message) {
                    swipeRefreshLayout.setRefreshing(false);
                    MyToast.myToast(getApplicationContext(),message);
                }

                @Override
                public void onSuccess(MZPZJLListBean commonEntity) {
                    swipeRefreshLayout.setRefreshing(false);
                    initShow(commonEntity.getData(),isAdd);

                }
            });
    }

    private void initShow(List<MZPZJLListBean.DataBean> data,boolean isAdd) {
        if (mAdapter!=null){
            mAdapter.setData(data);
            mAdapter.notifyDataSetChanged();
        }else {
            mAdapter = new MZPZJLListAdapter(data);
            mAdapter.setLoadState(2);
            recyclerView.setAdapter(mAdapter);
            mAdapter.setOnItemClickListener(new BaseLoadMoreAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    jumpDetailActivity(view,position);
                }
            });
            mAdapter.setOnItemLongClickListener(new BaseLoadMoreAdapter.OnItemLongClickListener() {
                @Override
                public void onItemLongClick(View view, int position) {
                    if (isCurUser)
                        showDeleteDialog(position);
                }
            });
        }
        if (isAdd){//排序按照时间最新排序。
           // recyclerView.smoothScrollToPosition(0);
        }else {
            recyclerView.scheduleLayoutAnimation();
        }
    }

    private void showDeleteDialog(int position) {
        CenterDefineDialog.getInstance("确认删除该记录?").setCallback(new CenterDefineDialog.Callback1<Integer>() {
            @Override
            public void run(Integer integer) {
                requestDelete(position);
            }
        }).showDialog(getSupportFragmentManager());
    }

    private void requestDelete(int position) {
        BeautyDefine.getOpenPageDefine(this).progressControl(new OpenPageDefine.ProgressController.Showder("加载中",false));
        requestPost(HttpUrls.URL_MZPZJL_DELETE(), new ApiParams().with("id", mAdapter.mDatas.get(position).getId()+""), CommonEntity.class, new HttpCallBack<CommonEntity>() {
            @Override
            public void onFailure(String message) {
                MyToast.myToast(getApplicationContext(),message);
                BeautyDefine.getOpenPageDefine(MZPZJLListActivity.this).progressControl(new OpenPageDefine.ProgressController.Hider());
            }

            @Override
            public void onSuccess(CommonEntity commonEntity) {
                mAdapter.mDatas.remove(position);
                mAdapter.notifyItemRemoved(position);
                mAdapter.notifyItemRangeChanged(position,mAdapter.mDatas.size());
                BeautyDefine.getOpenPageDefine(MZPZJLListActivity.this).progressControl(new OpenPageDefine.ProgressController.Hider());
            }
        });
    }

    private void jumpDetailActivity(View view,int position){
        Intent intent = new Intent(this, MZPZJLDetailActivity.class);
        intent.putExtra("data", (Serializable) mAdapter.mDatas.get(position)).putExtra("position",position)
                .putExtra("isCurUser",isCurUser);
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(
                    (this), true,
                    new Pair<>(view, getResources().getString(R.string.transition_fab_ClassRoom)),new Pair<>(toolbar,getResources().getString(R.string.transition_toolbar_ClassRoom)));
            ActivityCompat.startActivityForResult(this, intent,101, ActivityOptionsCompat.makeSceneTransitionAnimation(this,pairs).toBundle());
        } else {
            startActivityForResult(intent,101);
        }
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==100&&resultCode==100){
            requestData(true);
        }else if (requestCode==101&&resultCode==100){
            assert data != null;
            int pos = data.getIntExtra("position",0);
            mAdapter.mDatas.remove(pos);
            mAdapter.notifyItemRemoved(pos);
            mAdapter.notifyItemRangeChanged(pos,mAdapter.mDatas.size());
        }
    }
}
