package com.xingwang.classroom.ui.mzpzjl;


import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Transition;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beautydefinelibrary.BeautyDefine;
import com.beautydefinelibrary.OpenPageDefine;
import com.xingwang.classroom.R;
import com.xinwang.bgqbaselib.base.BaseNetActivity;
import com.xinwang.bgqbaselib.http.ApiParams;
import com.xinwang.bgqbaselib.http.CommonEntity;
import com.xinwang.bgqbaselib.http.HttpCallBack;
import com.xinwang.bgqbaselib.http.HttpUrls;
import com.xinwang.bgqbaselib.utils.AndroidBug5497Workaround;
import com.xinwang.bgqbaselib.utils.KeyBoardHelper;
import com.xinwang.bgqbaselib.utils.LogUtil;
import com.xinwang.bgqbaselib.utils.MyToast;
import com.xinwang.bgqbaselib.utils.TimeUtil;
import com.xinwang.bgqbaselib.view.CustomToolbar;


/**
 * Date:2021/10/12
 * Time;16:24
 * author:baiguiqiang
 */
public class MZPZJLAddActivity extends BaseNetActivity implements TextWatcher {
    private RelativeLayout rlRoot,rlContent;
    private EditText etTitle,etContent;
    private TextView tvTime;
    private ImageView ivSubmit;
    private KeyBoardHelper mKeyBoarHelper;
    private boolean isVisKeyBoard = false;
    @Override
    protected int layoutResId() {
        return R.layout.activity_mzpzjl_add_classroom;
    }
    private void onBack(){
        if (isVisKeyBoard) {
            if (mKeyBoarHelper != null)
                mKeyBoarHelper.closeInputMethodManager();
        }else {
            // targetSdkVersion 28以上bug 退出app 走onstop方法后 退出动画失效 只能修改 targetSdkVersion
            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                if (getWindow().getEnterTransition() != null)
                    getWindow().getEnterTransition().removeListener(mTransition);
                etContent.removeTextChangedListener(this);
                etTitle.removeTextChangedListener(this);
                ColorDrawable colorDrawable = new ColorDrawable();
                colorDrawable.setColor(getResources().getColor(R.color.themeClassRoom));
                colorDrawable.setAlpha(0);

                rlContent.setVisibility(View.GONE);
                getWindow().setBackgroundDrawable(colorDrawable);
                rlRoot.setBackgroundResource(R.drawable.mzpzjl_add_bg_back_classroom);
                Transition mTransition = getWindow().getReturnTransition();

                if (mTransition != null)
                    mTransition.addListener(new Transition.TransitionListener() {
                        @Override
                        public void onTransitionStart(Transition transition) {
                            LogUtil.i("onTransitionStart");
                        }

                        @Override
                        public void onTransitionEnd(Transition transition) {
                            mTransition.removeListener(this);
                        }

                        @Override
                        public void onTransitionCancel(Transition transition) {

                        }

                        @Override
                        public void onTransitionPause(Transition transition) {

                        }

                        @Override
                        public void onTransitionResume(Transition transition) {
                            rlRoot.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    rlRoot.getLayoutParams().width = rlRoot.getLayoutParams().height;
                                    rlRoot.setBackgroundResource(R.drawable.mzpzjl_add_bg_back_round_classroom);
                                }
                            }, 200);
                        }
                    });
                finishAfterTransition();


            } else {
                finish();
            }
            overridePendingTransition(0, 0);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mKeyBoarHelper!=null)
        mKeyBoarHelper.onDestroy();


    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidBug5497Workaround.assistActivity(this);
        mKeyBoarHelper=new KeyBoardHelper(this);
        mKeyBoarHelper.onCreate();
        initViews();
        initListener();
        initData();
    }

    private void initData() {
        tvTime.setText(TimeUtil.getYMD(TimeUtil.getCurTime0()/1000+""));

    }
    @TargetApi(android.os.Build.VERSION_CODES.LOLLIPOP)
    private Transition.TransitionListener mTransition =new Transition.TransitionListener(){

        @Override
        public void onTransitionStart(Transition transition) {
            rlContent.setVisibility(View.GONE);
        }

        @Override
        public void onTransitionEnd(Transition transition) {
            rlContent.setVisibility(View.VISIBLE);
            etTitle.requestFocus();
            mKeyBoarHelper.openInputMethodManager(etTitle);
        }

        @Override
        public void onTransitionCancel(Transition transition) {

        }

        @Override
        public void onTransitionPause(Transition transition) {

        }

        @Override
        public void onTransitionResume(Transition transition) {

        }
    };
    private void initListener() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            if (getWindow().getEnterTransition() != null)
                getWindow().getEnterTransition().addListener(mTransition);
        }
        etContent.addTextChangedListener(this);
        etTitle.addTextChangedListener(this);
        ((CustomToolbar)findViewById(R.id.toolbar)).setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBack();
            }
        });
        findViewById(R.id.rlTime).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseTime();
            }
        });
        ivSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goSubmitData();
            }
        });
        mKeyBoarHelper.setOnKeyBoardStatusChangeListener(new KeyBoardHelper.OnKeyBoardStatusChangeListener() {
            @Override
            public void OnKeyBoardChanged(boolean visible, int keyBoardHeight) {
                isVisKeyBoard = visible;
            }
        });
    }

    private void goSubmitData() {
        if (mKeyBoarHelper != null)
            mKeyBoarHelper.closeInputMethodManager();
        BeautyDefine.getOpenPageDefine(this).progressControl(new OpenPageDefine.ProgressController.Showder("加载中",false));
        requestPost(HttpUrls.URL_MZPZJL_CREATE(), new ApiParams().with("date_pz", tvTime.getText().toString()).with("title", etTitle.getText().toString()).with("tips",
                etContent.getText().toString()), CommonEntity.class, new HttpCallBack<CommonEntity>() {
            @Override
            public void onFailure(String message) {
                MyToast.myToast(getApplicationContext(),message);
                BeautyDefine.getOpenPageDefine(MZPZJLAddActivity.this).progressControl(new OpenPageDefine.ProgressController.Hider());
            }

            @Override
            public void onSuccess(CommonEntity commonEntity) {
                MyToast.myToast(getApplicationContext(),"创建成功");
                etContent.setText("");
                BeautyDefine.getOpenPageDefine(MZPZJLAddActivity.this).progressControl(new OpenPageDefine.ProgressController.Hider());
                setResult(100);
                onBack();
            }
        });
    }

    private void chooseTime() {
        String time = TimeUtil.getStringToDate(tvTime.getText().toString(),"yyyy-MM-dd")/1000+"";
        DatePickerDialog mDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                int mMonth =month+1;
                tvTime.setText(year + "-" + (mMonth<10?"0"+mMonth:mMonth) + "-" + dayOfMonth);
            }
        }, Integer.parseInt(TimeUtil.getY(time)),Integer.parseInt(TimeUtil.getM(time))-1,Integer.parseInt(TimeUtil.getD(time)));
        mDialog.show();
    }

    private void initViews() {
        rlRoot = findViewById(R.id.rlRoot);
        rlContent = findViewById(R.id.rlContent);
        etTitle = findViewById(R.id.etTitle);
        etContent = findViewById(R.id.etContent);
        tvTime = findViewById(R.id.tvTime);
        ivSubmit = findViewById(R.id.ivSubmit);
        ivSubmit.setEnabled(false);
    }

    @Override
    public void onBackPressed() {
        onBack();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        ivSubmit.setEnabled(!TextUtils.isEmpty(etContent.getText().toString())&&!TextUtils.isEmpty(etTitle.getText().toString()));
    }
}
