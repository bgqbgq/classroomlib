package com.xingwang.classroom.ui.offer;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.DashPathEffect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Utils;
import com.xingwang.classroom.R;
import com.xingwang.classroom.adapter.BaoJiaListAdapter;
import com.xingwang.classroom.bean.BaoJiaBean;
import com.xingwang.classroom.bean.OfferBean;
import com.xingwang.classroom.dialog.BottomChooseDialog;
import com.xingwang.classroom.dialog.BottomChooseMonAndYearDialog;
import com.xingwang.classroom.ui.statistic.StatisticPriceActivity;
import com.xingwang.classroom.ui.statistic.StatisticPriceFragment;
import com.xingwang.classroom.view.DetailsMarkerView;
import com.xingwang.classroom.view.MyLineChart;
import com.xingwang.classroom.view.PositionMarker;
import com.xingwang.classroom.view.RoundMarker;
import com.xingwang.classroom.view.WrapContentLinearLayoutManager;
import com.xinwang.bgqbaselib.adapter.BaseLoadMoreAdapter;
import com.xinwang.bgqbaselib.base.BaseLazyLoadFragment;
import com.xinwang.bgqbaselib.http.ApiParams;
import com.xinwang.bgqbaselib.http.HttpCallBack;
import com.xinwang.bgqbaselib.http.HttpUrls;
import com.xinwang.bgqbaselib.utils.Constants;
import com.xinwang.bgqbaselib.utils.CountUtil;
import com.xinwang.bgqbaselib.utils.GsonUtils;
import com.xinwang.bgqbaselib.utils.LogUtil;
import com.xinwang.bgqbaselib.utils.MyToast;
import com.xinwang.bgqbaselib.utils.TimeUtil;
import com.xinwang.bgqbaselib.view.DividerItemDecoration;
import com.xinwang.bgqbaselib.view.VpSwipeRefreshLayout;
import com.xinwang.bgqbaselib.view.loadmore.EndlessRecyclerOnScrollListener;
import com.xinwang.shoppingcenter.adapter.ShoppingGoodOrderListAdapter;
import com.xinwang.shoppingcenter.interfaces.OrderButtonListener;
import com.xinwang.shoppingcenter.ui.OrderDetailActivity;
import com.xinwang.shoppingcenter.ui.ReviewDetailActivity;
import com.xinwang.shoppingcenter.ui.ShoppingReviewActivity;
import com.xinwang.shoppingcenter.ui.WaybillDetailActivity;

import org.json.JSONException;
import org.json.JSONObject;


import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Date:2021/9/13
 * Time;16:34
 * author:baiguiqiang
 */
public class OfferDetailFragment extends BaseLazyLoadFragment {
    private MyLineChart mLineChar;
    private AppBarLayout appBarLayout;
    private TextView tvTime,tvUnit,tvPrice,tvPercentage,tvBJDDes;
    public String[] mUnits=new String[]{"元/吨","元/吨","元/公斤","元/斤","元/斤","元/斤","元/斤","元/斤"};//单位
    private String[] mCity=new String[]{"安徵","北京","重庆","福建","广东","甘肃","广西","贵州","海南","河北","河南","香港","黑龙","湖南","湖北","吉林","江苏","江西","辽宁","澳门",
            "内蒙","宁夏","青海","陕西","四川","山东","上海","山西","天津","台湾","新辐","西藏","云南","浙江"};
    private Button tvCity,tvMonth,tvYear;
    private int curPosition=0;

    private long startTime;
    private long endTime;
    public String provice;
    private int pageNum =10;
    private int curPage =1;
    private int maxDay = 60;//日期最大选择天数
    private OfferDetailActivity mActivity;
    private VpSwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerview;
    private JSONObject mTopData;//上年或者上月数据
    private BaoJiaListAdapter mAdapter;
    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_offer_detail_classroom,container,false);
        mLineChar =view.findViewById(R.id.mLineChar);
        tvCity =view.findViewById(R.id.tvCity);
        tvMonth =view.findViewById(R.id.tvMonth);
        tvYear =view.findViewById(R.id.tvYear);
        appBarLayout =view.findViewById(R.id.app_bar_layout);
        tvPercentage =view.findViewById(R.id.tvPercentage);
        tvTime =view.findViewById(R.id.tvTime);
        tvPrice =view.findViewById(R.id.tvPrice);
        tvUnit =view.findViewById(R.id.tvUnit);
        tvBJDDes =view.findViewById(R.id.tvBJDDes);
        recyclerview =view.findViewById(R.id.recyclerView);
        swipeRefreshLayout =view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setColorSchemeResources(R.color.SwipeRefreshLayoutClassRoom);
        swipeRefreshLayout.setOnRefreshListener(() -> goPercentageProData(null,0,0,0));
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
                swipeRefreshLayout.setEnabled(i>=0);
            }
        });
        if (getContext()!=null) {
            recyclerview.setLayoutManager(new WrapContentLinearLayoutManager(getContext()));
            recyclerview.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST));
        }
        recyclerview.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                if (mData.size()>0&&!isRequesting)
                    goRequestUserBaojia(Constants.LOAD_DATA_TYPE_MORE);

            }
        });
        return view;
    }

    @Override
    public void initData() {
        curPosition = getArguments().getInt("position");
        mActivity = (OfferDetailActivity) getActivity();
        endTime = TimeUtil.getCurTime0();
        startTime = TimeUtil.byTimeAddDay(endTime,-maxDay);
        provice = getArguments().getString("provice");
        tvCity.setText(provice);

        setTimeShow();
        initListener();
        setLineChart();
        goPercentageProData(null,0,0,0);
    }

    private void initShow() {
        tvTime.setText(TimeUtil.getMD(System.currentTimeMillis() / 1000 + "") + "更新");
        tvUnit.setText("单位：" + mUnits[curPosition]);
        tvPrice.setText(getPriceSpannable());
        tvPercentage.setText(getPercentageSpannable());

    }
    private void  initListener(){
        tvMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeChooseDialog(true);
            }
        });
        tvYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeChooseDialog(false);
            }
        });
        tvCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> mList = new ArrayList(Arrays.asList(mCity));
                BottomChooseDialog mDialog =  BottomChooseDialog.getInstance( mList,mList.indexOf(provice));
                mDialog.setCallback(new BottomChooseDialog.Callback1<Integer>() {
                    @Override
                    public void run(Integer s) {
                        swipeRefreshLayout.setRefreshing(true);
                        goPercentageProData(mList.get(s),0,0,2);
                    }
                });
                mDialog.showDialog(getFragmentManager());
            }
        });
    }
    private void showTimeChooseDialog(boolean isStartTime){
        String time = isStartTime?(startTime/1000+""):(endTime/1000+"");
        DatePickerDialog mDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                int mMonth =month+1;
                long chooseTime = TimeUtil.getStringToDate(year + "-" + mMonth + "-" + dayOfMonth, "yyyy-MM-dd");
                if (isStartTime){
                    if ((endTime-chooseTime)/1000/60/60/24>maxDay||endTime-chooseTime<0)
                        goPercentageProData(null,chooseTime,TimeUtil.byTimeAddDay(chooseTime,maxDay),1);
                    else
                        goPercentageProData(null,chooseTime,endTime,1);
                }else {
                    if ((chooseTime-startTime)/1000/60/60/24>maxDay||chooseTime-startTime<0){
                        goPercentageProData(null,TimeUtil.byTimeAddDay(chooseTime,-maxDay),chooseTime,1);
                    }else
                        goPercentageProData(null,startTime,chooseTime,1);
                }
            }
        }, Integer.parseInt(TimeUtil.getY(time)),Integer.parseInt(TimeUtil.getM(time))-1,Integer.parseInt(TimeUtil.getD(time)));
        if (!isStartTime) {
            mDialog.setTitle("截至时间");

        }else {
            mDialog.setTitle("开始时间");
        }

        mDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

        mDialog.show();

    }


    private void setTimeShow(){
        tvMonth.setText(TimeUtil.getYMD(startTime/1000+""));
        tvYear.setText(TimeUtil.getYMD(endTime/1000+""));

    }
    private SpannableString getPercentageSpannable() {
        String mP ="--";
        if (mPercentage<=0){
            tvPercentage.setTextColor(ContextCompat.getColor(getContext(),R.color.themeClassRoom));
            if (mPercentage!=0)
                mP=mPercentage+"";//负数有符号
        }else {

            mP="+"+mPercentage;
            tvPercentage.setTextColor(ContextCompat.getColor(getContext(),R.color.red));
        }

        SpannableString spannableString =new SpannableString(mP+"%");
        spannableString.setSpan(new RelativeSizeSpan(0.4f),spannableString.length()-1,spannableString.length(),SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    private SpannableString getPriceSpannable(){
        SpannableString spannableString =new SpannableString( mPrice+mUnits[curPosition]);
        spannableString.setSpan(new RelativeSizeSpan(0.4f),mPrice.length(),spannableString.length(),SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
        return spannableString;
    }
    /**
     * 填充数据
     */
    private void lineChartToData(JSONObject jsonObject) {

        //设置数据
        setData(jsonObject);
        //默认动画

        List<ILineDataSet> setsCubic = mLineChar.getData().getDataSets();
        mLineChar.animateX(getAnimateTime(setsCubic.size()));
        for (ILineDataSet iSet : setsCubic) {
            LineDataSet set = (LineDataSet) iSet;
            //set.setMode( LineDataSet.Mode.CUBIC_BEZIER);//曲线
            set.setDrawValues(false);//在图标上面不显示数值
            set.setMode( LineDataSet.Mode.LINEAR); //直线
        }
        //刷新
        mLineChar.invalidate();
        // 得到这个文字
        Legend l = mLineChar.getLegend();

        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setFormLineWidth(5f);
        l.setFormSize(5f);
        l.setXEntrySpace(16f);
        l.setFormToTextSpace(2f);
        l.setStackSpace(10f);
        // 修改文字 ...
        l.setForm(Legend.LegendForm.LINE);
    }
    private int getAnimateTime(int sum){// 250个数量 1000计算
        if (sum*4<200){
            return 200;
        }else if (sum*4<1000){
            return sum*4;
        }else {
            return 1000;
        }
    }
    private LineDataSet set1;
    private LineDataSet set2;
    private String mPrice;//平均价格
    private float mPercentage=0;//同期比列
    private void setData(JSONObject jsonObject) {
        float totalPrice=0;
        int totalTime=0;
        ArrayList<Entry> values =new ArrayList();
        ArrayList<Entry> values2 =new ArrayList<>();
        Iterator kes = jsonObject.keys();
        long curTime=0;
        while (kes.hasNext()){
            try {
                String key = (String) kes.next();
                float value = Float.parseFloat(jsonObject.getString(key));
                totalPrice=sum(totalPrice,value);
                curTime = TimeUtil.getStringToDate(key,"yyyy-MM-dd");
                values.add(new Entry(curTime/1000/60/60/24+1,value));
                String  nextTime=TimeUtil.getYMD(TimeUtil.byTimeAddYear(curTime,-1)/1000+"");//去年同期日期
                if (mTopData!=null&&mTopData.getString(nextTime)!=null)
                    values2.add(new Entry(curTime/1000/60/60/24+1, Float.parseFloat(mTopData.getString(nextTime))));

                totalTime +=1;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        if (values.size()==1){
            setValues(values,curTime,totalPrice);
            totalPrice=sum(totalPrice,totalPrice);
            totalTime +=1;
        }

        int mTotalPrice = (int) (totalPrice*100);
        int mTotalTime =totalTime*100;
        DecimalFormat df=new DecimalFormat("0.00");//保留为啥
        mPrice =df.format(((double)mTotalPrice)/mTotalTime);

        setPercentage();

        if (mLineChar.getData() != null && mLineChar.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mLineChar.getData().getDataSetByIndex(0);
            set1.setValues(values);
            set2 = (LineDataSet) mLineChar.getData().getDataSetByIndex(1);
            set2.setValues(values2);
            mLineChar.getData().notifyDataChanged();
            mLineChar.notifyDataSetChanged();
        } else {

            // mLineChar.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);//x 显示在表格下方
            mLineChar.getXAxis().setValueFormatter(new ValueFormatter(){//设置显示x轴为时间
                @Override
                public String getFormattedValue(float value) {
                    try {
                        return TimeUtil.getMD(((int)value)*60*60*24+"");
                    }catch (Exception e){
                        return super.getFormattedValue(value);
                    }
                }
            });
            // 创建一个数据集,并给它一个类型
            set1 = new LineDataSet(values,"\n今年");

            // 在这里设置线
           /* set1.enableDashedLine(10f, 5f, 0f);
            set1.enableDashedHighlightLine(10f, 5f, 0f);*/
            set1.setColor(ContextCompat.getColor(getContext(),R.color.themeClassRoom));
            set1.setCircleColor(ContextCompat.getColor(getContext(),R.color.themeClassRoom));
            set1.setLineWidth(1f);
            set1.setCircleRadius(2f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setDrawFilled(false);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            set2 = new LineDataSet(values2,"\n去年同期");
            set2.setColor(ContextCompat.getColor(getContext(),R.color.red));
            set2.setCircleColor(ContextCompat.getColor(getContext(),R.color.red));
            set2.setLineWidth(1f);
            set2.setCircleRadius(2f);
            set2.setDrawCircleHole(false);
            set2.setValueTextSize(9f);
            set2.setDrawFilled(false);
            set2.setFormLineWidth(1f);
            set2.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set2.setFormSize(15.f);

            mLineChar.getAxisRight().setEnabled(false); //设置图表右边的y轴禁用
            //隐藏左边坐标轴横网格线
            mLineChar.getAxisLeft().setDrawGridLines(false);
            //隐藏右边坐标轴横网格线
            mLineChar.getAxisRight().setDrawGridLines(false);
            //隐藏X轴竖网格线
            mLineChar.getXAxis().setDrawGridLines(false);
            mLineChar.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);//x位置
            mLineChar.setExtraBottomOffset(10f);//设置legend 和X轴之间间距
            /*  if (Utils.getSDKInt() >= 18) {*/
            // 填充背景只支持18以上
            //Drawable drawable = ContextCompat.getDrawable(this, R.mipmap.ic_launcher);
            //set1.setFillDrawable(drawable);
               /* set1.setFillColor(ContextCompat.getColor(getContext(),R.color.theme1ClassRoom));
                set2.setFillColor(ContextCompat.getColor(getContext(),R.color.theme1ClassRoom));*/
            /*} else {
                set1.setFillColor(ContextCompat.getColor(getContext(),R.color.themeClassRoom));
            }*/


            ArrayList<ILineDataSet> dataSets = new ArrayList();
            //添加数据集
            dataSets.add(set1);
            dataSets.add(set2);

            //创建一个数据集的数据对象
            LineData data = new LineData(dataSets);

            //谁知数据
            mLineChar.setData(data);
        }
    }

    private void setValues(ArrayList<Entry> values, long curTime, float totalPrice) {
        if (curTime<endTime){
            values.add(new Entry( endTime/1000/60/60/24+1,totalPrice));
        }else {
            values.add(0,new Entry( startTime/1000/60/60/24+1,totalPrice));
        }
    }


    private  void setPercentage() {

        try {
            float totalPrice=0;
            int totalTime=0;
            Iterator kes = mTopData.keys();
            while (kes.hasNext()){

                String key = (String) kes.next();
                float value = Float.parseFloat(mTopData.getString(key));

                totalPrice=sum(totalPrice,value);
                totalTime+=1;
            }

            int mTotalPrice= CountUtil.multiplyByInt(100,totalPrice);
            int mTotalTime =totalTime*100;
            DecimalFormat df=new DecimalFormat("0.00");//保留为啥
            String mOldPrice =df.format((float) mTotalPrice/mTotalTime);

            Float old = Float.parseFloat(mOldPrice);
            Float cur = Float.parseFloat(mPrice);
            Double p3 =  Double.parseDouble((cur-old) / old+"");

            mPercentage =Float.parseFloat(df.format(p3*100));
        }catch (Exception e){
            mPercentage=0;
        }

    }


    public float sum(float d1,float d2){
        BigDecimal bd1 = new BigDecimal(Float.toString(d1));
        BigDecimal bd2 = new BigDecimal(Float.toString(d2));
        return bd1.add(bd2).floatValue();
    }
    private void setLineChart() {
        //设置手势滑动事件
        // mLineChar.setOnChartGestureListener(this);
        //设置数值选择监听
        mLineChar.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                //查看覆盖物是否被回收
                if (mLineChar.isMarkerAllNull()) {
                    //重新绑定覆盖物
                    createMakerView();
                    //并且手动高亮覆盖物
                    mLineChar.highlightValue(h);
                }
            }

            @Override
            public void onNothingSelected() {

            }
        });
        //后台绘制
        mLineChar.setDrawGridBackground(false);
        //设置描述文本
        mLineChar.getDescription().setEnabled(false);
        //设置支持触控手势
        mLineChar.setTouchEnabled(true);
        //设置缩放
        mLineChar.setDragEnabled(false);
        //设置推动
        mLineChar.setScaleEnabled(false);
        //如果禁用,扩展可以在x轴和y轴分别完成
        mLineChar.setPinchZoom(true);
        mLineChar.setHighlightPerTapEnabled(true);
        mLineChar.setHighlightPerDragEnabled(true);
        mLineChar.setDragEnabled(true);//拖动高亮

       /* //透明化图例
        Legend legend = mLineChar.getLegend();
        legend.setForm(Legend.LegendForm.NONE);
        legend.setTextColor(Color.WHITE);*/
      /*  DetailsMarkerView detailsMarkerView = new DetailsMarkerView(getContext(),R.layout.layout_marker_view_detail,mUnit,startData);
        //一定要设置这个玩意，不然到点击到最边缘的时候不会自动调整布局
        detailsMarkerView.setChartView(mLineChar);
        mLineChar.setMarker(detailsMarkerView);*/
        createMakerView();
    }
    /**
     * 创建覆盖物
     */
    public void createMakerView() {
        DetailsMarkerView detailsMarkerView = new DetailsMarkerView(getContext(), R.layout.layout_marker_view_detail, mUnits[curPosition]);
        detailsMarkerView.setChartView(mLineChar);
        mLineChar.setDetailsMarkerView(detailsMarkerView);
        mLineChar.setPositionMarker(new PositionMarker(getContext()));
        mLineChar.setRoundMarker(new RoundMarker(getContext()));
    }
    public static OfferDetailFragment getInstance(int position,String provice){
        OfferDetailFragment mFragment = new OfferDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position",position);
        bundle.putString("provice",provice);
        mFragment.setArguments(bundle);
        return mFragment;
    }

    /**
     *
     * @param content 可以是时间 省份 null不更新
     * @param type  0 null  1 时间 2 省份
     */
    private void goRequestData(String content,long mStartTime ,long mEndTime,int type){

        requestGet(HttpUrls.URL_BAOJIA_LISTS(),new ApiParams().with("provice",type==2?content:provice)
                .with("start_date",TimeUtil.getYMD(type==1?(mStartTime/1000+""):(startTime/1000+""))).with("end_date",
                        TimeUtil.getYMD(type==1?(mEndTime/1000+""):(endTime/1000+""))).with("type",mActivity.types[curPosition]), OfferBean.class, new HttpCallBack<OfferBean>() {

            @Override
            public void onFailure(String message) {
                swipeRefreshLayout.setRefreshing(false);
                MyToast.myToast(mActivity.getApplicationContext(),message);
            }

            @Override
            public void onSuccess(OfferBean historyPriceBean) {
                swipeRefreshLayout.setRefreshing(false);
                try {
                    JSONObject mJsonObject = new JSONObject(GsonUtils.createGsonString(historyPriceBean.getData()));
                    if (type==1) {
                        startTime = mStartTime;
                        endTime =mEndTime;
                        setTimeShow();
                        if (tvBJDDes.getVisibility()!=View.VISIBLE)
                            goRequestUserBaojia(Constants.LOAD_DATA_TYPE_REFRESH);
                    }else if (type==2){
                        provice =content;
                        tvCity.setText(provice);
                        curPage=1;
                        goRequestUserBaojia(Constants.LOAD_DATA_TYPE_REFRESH);
                    }else {
                        curPage=1;
                        goRequestUserBaojia(Constants.LOAD_DATA_TYPE_REFRESH);
                    }
                    mLineChar.clear();

                    //  mJsonObject =new JSONObject("{\"2021-09-9\":2348,\"2021-09-10\":2308,\"2021-09-11\":2358,\"2021-09-12\":2358,\"2021-09-13\":2398,\"2021-09-14\":2218,\"2021-09-15\":2258,\"2021-09-16\":2258,\"2021-09-17\":2288}");
                    lineChartToData(mJsonObject);
                    initShow();
                }catch (JSONException e){
                    e.printStackTrace();
                    if (type==1) {
                        startTime = mStartTime;
                        endTime =mEndTime;
                        setTimeShow();
                    }else if (type==2){
                        provice =content;
                        tvCity.setText(provice);
                        //情况用户报价
                        mData.clear();
                        initAdapter(2);
                    }else {
                        if (mAdapter==null){
                            initAdapter(2);
                        }
                    }
                    mLineChar.clear();
                    mPrice ="--";
                    tvPrice.setText(getPriceSpannable());
                    mPercentage=0f;
                    tvPercentage.setText(getPercentageSpannable());
                    MyToast.myToast(mActivity,"暂未查找到历史数据");
                }
            }
        });
    }
    private List<BaoJiaBean.DataBean> mData =new ArrayList<>();
    boolean isRequesting =false;
    private void goRequestUserBaojia(int loadDataTypeInit){
        isRequesting =true;
        requestGet(HttpUrls.URL_BAOJIA_BAOJIA(),new ApiParams().with("provice",provice)
                .with("type",mActivity.types[curPosition])
                .with("page",curPage).with("page_num",pageNum), BaoJiaBean.class, new HttpCallBack<BaoJiaBean>() {

            @Override
            public void onFailure(String message) {
                isRequesting =false;
                MyToast.myToast(mActivity.getApplicationContext(),message);
            }

            @Override
            public void onSuccess(BaoJiaBean baoJiaBean) {
                if (loadDataTypeInit!= Constants.LOAD_DATA_TYPE_MORE){
                    mData.clear();
                }
                mData.addAll(baoJiaBean.getData());
                curPage++;
                if(baoJiaBean.getData().size()<pageNum){
                    initAdapter(loadDataTypeInit!= Constants.LOAD_DATA_TYPE_MORE&&baoJiaBean.getData().size()==0?2:3);
                }else {
                    initAdapter(1);
                }
                isRequesting =false;
            }
        });
    }
    /**
     *
     * @param content 可以是时间 省份 null不更新
     * @param type  0 null  1 时间 2 省份
     */
    public void goPercentageProData(String content,long mStartTime ,long mEndTime,int type){
        long percentStartTime = type==1?TimeUtil.byTimeAddYear(mStartTime,1):TimeUtil.byTimeAddYear(startTime,1);
        long percentEndTime = type==1?TimeUtil.byTimeAddYear(mEndTime,1):TimeUtil.byTimeAddYear(endTime,1);
        requestGet(HttpUrls.URL_BAOJIA_LISTS(),new ApiParams().with("provice",type==2?content:provice)
                .with("start_date",TimeUtil.getYMD(percentStartTime/1000+"")).with("end_date",TimeUtil.getYMD(percentEndTime/1000+"")).with("type",mActivity.types[curPosition]), OfferBean.class, new HttpCallBack<OfferBean>() {

            @Override
            public void onFailure(String message) {
                swipeRefreshLayout.setRefreshing(false);
                MyToast.myToast(mActivity.getApplicationContext(),message);
            }

            @Override
            public void onSuccess(OfferBean historyPriceBean) {
                try {
                    mTopData =new JSONObject(GsonUtils.createGsonString(historyPriceBean.getData()));
                } catch (JSONException e) {
                    e.printStackTrace();
                    mTopData =null;
                }
                goRequestData( content, mStartTime , mEndTime,type);

            }
        });
    }
    private void initAdapter(int state){
        if (state==2){
            tvBJDDes.setVisibility(View.GONE);
        }else {
            tvBJDDes.setVisibility(View.VISIBLE);
        }
        if (mAdapter==null) {
            mAdapter = new BaoJiaListAdapter(mData,mUnits[curPosition]);
            mAdapter.setLoadStateNoNotify(state);
            recyclerview.setAdapter(mAdapter);

        }else {
            mAdapter.setLoadStateNoNotify(state);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
