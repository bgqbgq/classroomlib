package com.xingwang.classroom.ui.mzpzjl;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beautydefinelibrary.BeautyDefine;
import com.beautydefinelibrary.OpenPageDefine;
import com.xingwang.classroom.R;
import com.xingwang.classroom.bean.MZPZJLListBean;
import com.xinwang.bgqbaselib.base.BaseNetActivity;
import com.xinwang.bgqbaselib.dialog.CenterDefineDialog;
import com.xinwang.bgqbaselib.http.ApiParams;
import com.xinwang.bgqbaselib.http.CommonEntity;
import com.xinwang.bgqbaselib.http.HttpCallBack;
import com.xinwang.bgqbaselib.http.HttpUrls;
import com.xinwang.bgqbaselib.utils.MyToast;
import com.xinwang.bgqbaselib.utils.TimeUtil;
import com.xinwang.bgqbaselib.view.CustomToolbar;

/**
 * Date:2021/10/14
 * Time;16:12
 * author:baiguiqiang
 */
public class MZPZJLDetailActivity extends BaseNetActivity {
    private TextView tvTip,tvNum,tvTime;
    private MZPZJLListBean.DataBean mData;
    private LinearLayout llContent;



    @Override
    protected int layoutResId() {
        return R.layout.activity_mzpzjl_detail_classroom;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mData = (MZPZJLListBean.DataBean) getIntent().getSerializableExtra("data");

        if (mData==null)
            mData =new MZPZJLListBean.DataBean();
        initViews();
        initShow();
        initListener();
        initListData();
    }

    private void initListener() {
        ((CustomToolbar)findViewById(R.id.toolbar)).setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBack();
            }
        });

    }
    private void showDeleteDialog() {
        CenterDefineDialog.getInstance("确认删除该记录?").setCallback(new CenterDefineDialog.Callback1<Integer>() {
            @Override
            public void run(Integer integer) {
                requestDelete();
            }
        }).showDialog(getSupportFragmentManager());
    }

    private void requestDelete() {
        BeautyDefine.getOpenPageDefine(this).progressControl(new OpenPageDefine.ProgressController.Showder("加载中",false));
        requestPost(HttpUrls.URL_MZPZJL_DELETE(), new ApiParams().with("id", mData.getId()+""), CommonEntity.class, new HttpCallBack<CommonEntity>() {
            @Override
            public void onFailure(String message) {
                MyToast.myToast(getApplicationContext(),message);
                BeautyDefine.getOpenPageDefine(MZPZJLDetailActivity.this).progressControl(new OpenPageDefine.ProgressController.Hider());
            }

            @Override
            public void onSuccess(CommonEntity commonEntity) {
                BeautyDefine.getOpenPageDefine(MZPZJLDetailActivity.this).progressControl(new OpenPageDefine.ProgressController.Hider());
                Intent intent= new Intent().putExtra("position",getIntent().getIntExtra("position",0));
                setResult(100,intent);
                onBack();

            }
        });
    }
    @Override
    public void onBackPressed() {
        onBack();
    }

    private void onBack() {
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            finishAfterTransition();
        }else {
            finish();
        }
        overridePendingTransition(0, 0);
    }

    private void initListData() {
        llContent.removeAllViews();
        long curTime = TimeUtil.getCurTime0()/1000;
        for (int i = 0; i < mData.getEvents().size(); i++) {
            View view = LayoutInflater.from(this).inflate(R.layout.item_waybill_detail_shoppingcenter, llContent, false);
            TextView tvTime = view.findViewById(com.xinwang.shoppingcenter.R.id.tvTime);
            TextView tvContent = view.findViewById(com.xinwang.shoppingcenter.R.id.tvContent);
            View viewPos = view.findViewById(com.xinwang.shoppingcenter.R.id.viewPos);

            if (curTime==TimeUtil.getStringToDate(mData.getEvents().get(i).getEvent_date(),"yyyy-MM-dd")/1000){
                tvTime.setTextColor(ContextCompat.getColor(this,R.color.themeClassRoom));
                tvContent.setTextColor(ContextCompat.getColor(this,R.color.themeClassRoom));
                viewPos.setSelected(true);
            }
            tvTime.setText(mData.getEvents().get(i).getEvent_date());
            tvContent.setText(mData.getEvents().get(i).getEvent_info());
            if (i == 0) {
                view.findViewById(com.xinwang.shoppingcenter.R.id.view1).setVisibility(View.INVISIBLE);
            } else if (i == mData.getEvents().size() - 1) {
                view.findViewById(com.xinwang.shoppingcenter.R.id.view2).setVisibility(View.INVISIBLE);
            }
            llContent.addView(view);
            llContent.setVisibility(View.VISIBLE);
        }
    }

    private void initShow() {
        tvTip.setText(mData.getTitle());
        tvNum.setText("");
        tvTime.setText(mData.getDate_pz());

        if (!getIntent().getBooleanExtra("isCurUser",true)){
            findViewById(R.id.ivDelete).setVisibility(View.GONE);
        }else {
            findViewById(R.id.ivDelete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDeleteDialog();
                }
            });
        }
    }

    private void initViews() {
        tvTip =findViewById(R.id.tvTip);
        tvNum =findViewById(R.id.tvNum);
        tvTime =findViewById(R.id.tvTime);
        llContent =findViewById(R.id.llContent);
    }
}
