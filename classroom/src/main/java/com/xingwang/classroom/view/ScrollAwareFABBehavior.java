package com.xingwang.classroom.view;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.util.AttributeSet;
import android.view.View;



/**
 * Date:2021/10/12
 * Time;14:20
 * author:baiguiqiang
 */
public class ScrollAwareFABBehavior extends FloatingActionButton.Behavior {
    private FastOutLinearInInterpolator folistener = new FastOutLinearInInterpolator();
    public ScrollAwareFABBehavior(Context context, AttributeSet attributeSet){
        super();
    }
    @Override
    public boolean onStartNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull FloatingActionButton child, @NonNull View directTargetChild, @NonNull View target, int axes, int type) {
        return axes== ViewCompat.SCROLL_AXIS_VERTICAL||super.onStartNestedScroll(coordinatorLayout, child, directTargetChild, target, axes, type);
    }
    private boolean isAnimatingOut = false;
    @SuppressLint("RestrictedApi")
    @Override
    public void onNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull FloatingActionButton child, @NonNull View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed, int type) {

        if (dyConsumed>0&&!isAnimatingOut&&child.getVisibility() ==View.VISIBLE){
            animateOut(child);
        }else if (dyConsumed<0&&child.getVisibility()!=View.VISIBLE){
            animateIn(child);
        }
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, type);
    }
    private void  animateIn(FloatingActionButton child){

        ViewCompat.animate(child).translationY(0).setInterpolator(folistener).setListener(null).start();
        child.setVisibility(View.VISIBLE);
    }
    private void animateOut(FloatingActionButton child){
        ViewCompat.animate(child).translationY(child.getHeight()).setInterpolator(folistener).setListener(new ViewPropertyAnimatorListener(){

            @Override
            public void onAnimationStart(View view) {
                isAnimatingOut =true;
            }

            @Override
            public void onAnimationEnd(View view) {
                view.setVisibility(View.INVISIBLE);
                isAnimatingOut =false;
            }

            @Override
            public void onAnimationCancel(View view) {

                isAnimatingOut =false;
            }
        }).start();
    }
}